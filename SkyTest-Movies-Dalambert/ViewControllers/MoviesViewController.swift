//
//  MoviesViewController.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 16/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit
import ObjectMapper

class MoviesViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView?
    
    let application = MoviesApplication()
    
    var datasource: [LoadMovieListModel]? = []
    
    var selectedData: LoadMovieListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoadingView.shared()?.show(self.view!)
        
        self.application.LoadMovieList(success: { (result: [Mappable]?) in

            self.datasource = (result as! [LoadMovieListModel])
            
            LoadingView.shared()?.hide()
            
            self.collectionView!.reloadData()
            
        }, error: { (result: String?) in
            
            self.alert("Erro de conexão", result)
            
            LoadingView.shared()?.hide()
        })
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "nextSegue" {
            
            let destination = segue.destination as! DetailsViewController
            
            destination.datasource = self.selectedData
        }
    }
    
    //MARK: UICollectionViewDelegateFlowLayout methods
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((UIApplication.shared.keyWindow?.frame.size.width)! - 16) / 2, height: 270.0 )
    }
    
    //MARK: CollectionViewDelegate / Datasource methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedData = self.datasource![indexPath.item]
        
        self.performSegue(withIdentifier: "nextSegue", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (datasource?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "MoviesCollectionViewCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! MoviesCollectionViewCell
        
        cell.configure(model: self.datasource![indexPath.item])
        
        return cell
    }
    
    //MARK: IBAction methods
    
    @IBAction func backButtonPressed(_ button: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

}
