//
//  DetailsViewController.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 16/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit

class DetailsViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var spinner: UIActivityIndicatorView?
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var infoLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    
    var datasource: LoadMovieListModel?
    var selectedImageUrl: String?
    var selectedId: String?
    var id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.id = datasource?.id
        
        self.imageView?.image = nil
        
        self.titleLabel?.text = datasource?.title
        
        self.infoLabel?.text = "Ano: \(datasource?.release_year ?? "") / Duração: \(datasource?.duration ?? "")"
        
        self.descriptionLabel?.text = datasource?.overview
        
        self.spinner?.startAnimating()
        
        ImageCache.shared.loadImage(url: datasource?.cover_url,
                                    id: datasource?.id,
                                    success: { (image: UIImage?, result_id: String?) in
                                        
                                        self.spinner?.stopAnimating()
                                        
                                        if result_id != self.id {
                                            
                                            self.imageView?.image = nil
                                            
                                            return
                                        }
                                        
                                        self.imageView?.image = image
                                        
        }) { (errorImage: UIImage?, result_id: String?, response: String?) in
            
            self.spinner?.stopAnimating()
            
            if result_id == self.id {
                
                self.imageView?.image = errorImage
            }
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "thumbSegue" {
            
            let destination = segue.destination as! PreviewViewController
            
            destination.url = self.selectedImageUrl
            
            destination.id = self.selectedId
        }
    }
    
    //MARK: UICollectionViewDelegateFlowLayout methods
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200.0, height: 128.0)
    }
    
    //MARK: CollectionViewDelegate / Datasource methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedImageUrl = self.datasource!.backdrops_url![indexPath.item]
        
        self.selectedId = "\(self.datasource?.id ?? "")-\(indexPath.item)"
        
        self.performSegue(withIdentifier: "thumbSegue", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (datasource?.backdrops_url?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "DetailsThumbCollectionViewCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! DetailsThumbCollectionViewCell
        
        cell.configure(url: self.datasource!.backdrops_url![indexPath.item],
                       id: "\(self.datasource?.id ?? "")-\(indexPath.item)")
        
        return cell
    }

    //MARK: IBAction methods
    
    @IBAction func backButtonPressed(_ button: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

}
