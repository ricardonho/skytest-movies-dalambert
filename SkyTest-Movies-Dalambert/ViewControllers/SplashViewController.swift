//
//  SplashViewController.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 16/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //apenas um exemplo de splash screen, com um trigger
        //que envia para a proxima tela depois de realizar tarefas
        //respectivas à inicialização do app.
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            
            self.performSegue(withIdentifier: "nextSegue", sender: self)
        })
    }

}
