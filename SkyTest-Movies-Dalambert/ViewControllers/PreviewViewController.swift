//
//  PreviewViewController.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit

class PreviewViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var spinner: UIActivityIndicatorView?
    
    var url: String?
    var id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ImageCache.shared.loadImage(url: url,
                                    id: id,
                                    success: { (image: UIImage?, result_id: String?) in
                                        
                                        self.spinner?.stopAnimating()
                                        
                                        if result_id != self.id {
                                            
                                            self.imageView?.image = nil
                                            
                                            return
                                        }
                                        
                                        self.imageView?.image = image
                                        
        }) { (errorImage: UIImage?, result_id: String?, response: String?) in
            
            self.spinner?.stopAnimating()
            
            if result_id == self.id {
                
                self.imageView?.image = errorImage
            }
        }
    }
    
    // MARK: - IBAction methods
    
    @IBAction func closeButtonPressed(_ button: UIButton) {
        
        self.dismiss(animated: true) {
            
        }
    }

}
