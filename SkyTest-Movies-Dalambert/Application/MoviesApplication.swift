//
//  MoviesApplication.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit
import ObjectMapper

class MoviesApplication: NSObject {

    var moviesProxy: MoviesProxy = MoviesProxy()
    
    public func LoadMovieList(success: (([Mappable]?) -> Void)?,
                              error: ((String?) -> Void)?) {
        
        moviesProxy.LoadMovies(success: { (response: String?) in
            
            let result = Mapper<LoadMovieListModel>().mapArray(JSONString: response!)
            
            success!(result)
            
        }, error: { (response: String?) in
        
            error!(response)
        })
    }
}
