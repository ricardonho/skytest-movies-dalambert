//
//  BaseProxy.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class BaseProxy: NSObject {

    func Get(_ path: String?,
             success: ((String?) -> Void)?,
             error: ((String?) -> Void)?) {
        
        
        guard let url = URL(string: path!) else {
            
            error!(nil)
            
            return
        }
        
        Alamofire.request(url,
                          method: .get)
            .validate()
            .responseString(completionHandler: { (response: DataResponse<String>) in
               
                if response.response != nil {
                
                    success!(response.value)
                    
                } else {
                    
                    error!("Não foi possível estabelecer uma conexão, tente novamente mais tarde.")
                }
            })
    }
}
