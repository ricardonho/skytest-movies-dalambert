//
//  MoviesProxy.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class MoviesProxy: BaseProxy {

    func LoadMovies(success: ((String?) -> Void)?,
                    error: ((String?) -> Void)?) {
        
        Get("https://sky-exercise.herokuapp.com/api/Movies",
            success: success,
            error: error)
    }
}
