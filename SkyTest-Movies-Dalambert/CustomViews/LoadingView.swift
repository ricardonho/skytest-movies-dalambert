//
//  LoadingView.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    static var instance: LoadingView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.configure()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.configure()
    }
    
    func configure() {
        
        let blankView = UIView(frame: (UIApplication.shared.keyWindow?.frame)!)
        
        
        
        let loadingViewContainer = UIView(frame: CGRect(x: 0,
                                                        y: 0,
                                                        width: 92,
                                                        height: 92))
        
        let spinner = UIActivityIndicatorView()
        
        blankView.backgroundColor = UIColor.clear
        blankView.isUserInteractionEnabled = false
        
        loadingViewContainer.center = CGPoint(x: blankView.frame.size.width / 2,
                                              y: blankView.frame.size.height / 2)

        loadingViewContainer.layer.cornerRadius = 10.0
        loadingViewContainer.alpha = 0.5
        loadingViewContainer.backgroundColor = UIColor.darkGray
        
        spinner.center = CGPoint(x: blankView.frame.size.width / 2,
                                 y: blankView.frame.size.height / 2)
        
        spinner.style = .whiteLarge
        
        spinner.startAnimating()
        
        blankView.addSubview(loadingViewContainer)
        
        blankView.addSubview(spinner)
        
        
        
        self.addSubview(blankView)
    }
    
    static func shared() -> LoadingView? {
        
        if instance == nil {
            instance = LoadingView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 0, height: 0)))
        }
        
        return instance
    }
    
    func show(_ view: UIView?) {
        
        self.frame = (UIApplication.shared.keyWindow?.frame)!
        
        view?.addSubview(self)
    }
    
    func hide() {
        
        self.removeFromSuperview()
    }

}
