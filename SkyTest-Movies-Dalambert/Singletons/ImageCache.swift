//
//  ImageCache.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ImageCache: NSObject {

    static var shared = ImageCache()
    
    var cache: [String : UIImage?] = [:]
    var failed: [String] = []
    
    let errorImage = UIImage(named:"404")
    
    private override init() {
        
    }
    
    func loadImage(url: String?,
                   id: String?,
                   success: ((UIImage?, String?) -> Void)?,
                   error: ((UIImage?, String?, String?) -> Void)?) {
        
        if failed.contains(id!) {
            
            error!(errorImage, id, "Erro no carregamento da imagem")
            
            return
        }
        
        //imagem já encontra-se no buffer
        if cache.keys.contains(id!) {
            
            if cache[id!] != nil {
                
                success!(cache[id!]!, id)
                
                return
            }
        }
        
        Alamofire.request(url!).responseImage { response in

            if let image = response.result.value {
               
                self.cache[id!] = image
                
                success!(image, id)
                
            } else {
                
                self.failed.append(id!)
                
                error!(self.errorImage, id, "Erro no carregamento da imagem")
            }
        }
        
    }
}
