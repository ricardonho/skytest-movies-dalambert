//
//  DetailsThumbCollectionViewCell.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit

class DetailsThumbCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewError: UIImageView?
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var spinner: UIActivityIndicatorView?
    
    
    var id: String?
    
    func configure(url: String?, id: String?) {
        
        self.id = id
        
        self.imageView?.image = nil
        
        self.imageViewError?.isHidden = true
        
        self.spinner?.startAnimating()
        
        ImageCache.shared.loadImage(url: url,
                                    id: id,
                                    success: { (image: UIImage?, result_id: String?) in
                                        
                                        self.spinner?.stopAnimating()
                                        
                                        if result_id == self.id {
                                            
                                            self.imageView?.image = image
                                        }
                                        
        }) { (errorImage: UIImage?, result_id: String?, response: String?) in
            
            self.spinner?.stopAnimating()
            
            if result_id == self.id {
                
                self.imageViewError?.isHidden = false
                
                self.imageViewError?.image = errorImage
            }
        }
    }
    
}
