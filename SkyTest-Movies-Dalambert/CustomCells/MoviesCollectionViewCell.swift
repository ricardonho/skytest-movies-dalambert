//
//  MoviesCollectionViewCell.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewError: UIImageView?
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var spinner: UIActivityIndicatorView?
    
    
    var id: String?
    
    func configure(model: LoadMovieListModel?) {
        
        self.id = model?.id
        
        self.imageView?.image = nil
        
        self.label?.text = ""
        
        self.imageViewError?.isHidden = true
        
        self.spinner?.startAnimating()

        ImageCache.shared.loadImage(url: model?.cover_url,
                                    id: model?.id,
                                    success: { (image: UIImage?, result_id: String?) in
                                     
                                        self.spinner?.stopAnimating()
                                        
                                        if result_id == self.id {
                                            
                                            self.imageView?.image = image
                                        }

                                        self.label?.text = model?.title
                                        
        }) { (errorImage: UIImage?, result_id: String?, response: String?) in
            
            self.spinner?.stopAnimating()
            
            if result_id == self.id {
                
                self.imageViewError?.isHidden = false
                
                self.imageViewError?.image = errorImage
                
                self.label?.text = model?.title
            }
        }
    }
}
