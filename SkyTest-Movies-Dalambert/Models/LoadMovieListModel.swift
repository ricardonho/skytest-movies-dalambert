//
//  LoadMovieListModel.swift
//  SkyTest-Movies-Dalambert
//
//  Created by Ricardo d´Alambert on 17/06/2019.
//  Copyright © 2019 Liquisoft Corporate ltda. All rights reserved.
//

import UIKit
import ObjectMapper

public class LoadMovieListModel: Mappable {
    
    public var title: String?
    public var overview: String?
    public var duration: String?
    public var release_year: String?
    public var cover_url: String?
    public var backdrops_url: [String]?
    public var id: String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        self.title <- map["title"]
        self.overview <- map["overview"]
        self.duration <- map["duration"]
        self.release_year <- map["release_year"]
        self.cover_url <- map["cover_url"]
        self.backdrops_url <- map["backdrops_url"]
        self.id <- map["id"]
    }
    

    
    
}
